﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="HistorialFacturas.aspx.cs" Inherits="SantaNaturaNetworkV3.HistorialFacturas1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="assets/css/demo.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <style>
        .swal2-checkbox, .swal2-radio {
            display: none !important;
        }

        label {
            display: flex !important;
            align-items: baseline;
            justify-content: space-evenly;
        }

        #bloqueMayor {
            display: flex;
            justify-content: space-around;
            margin-top: 160px;
            flex-wrap: wrap
        }

        .bajarCombo {
            margin-top: 0px !important;
        }

        .content-table {
            border-collapse: collapse;
            margin: 25px 0;
            font-size: 0.9em;
            min-width: 400px;
            border-radius: 5px 5px 0 0;
            overflow: hidden;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
        }

            .content-table thead tr {
                background-color: #009897;
                color: #ffffff;
                text-align: left;
                font-weight: bold;
            }

            .content-table th, .content-table td {
                padding: 12px 15px !important;
            }

            .content-table tbody tr {
                border-bottom: 1px solid #dddddd;
            }

                .content-table tbody tr:nth-of-type(even) {
                    background-color: #f3f3f3;
                    font-weight: bold;
                    color: #009897;
                }

                .content-table tbody tr:last-of-type {
                    border-bottom: 2px solid #009897;
                }

                .content-table tbody tr.active-row {
                    font-weight: bold;
                    color: #009897;
                }

        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(img/loadingPageSantanatura.gif) center no-repeat #fff;
        }

        .container2 {
            max-width: 400px !important;
        }

        .select2-container {
            width: 100% !important;
        }

        .btn-ft{
            font-size: 1.7rem !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="css/file-upload.css" />
    <link rel="stylesheet" href="css/bootstrapv2.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box box-success">
        <h1 style="text-align: center; margin-bottom: 40px; font-family: -webkit-body; margin-top: 120px;">HISTORIAL DE FACTURAS DE COMISIONES</h1>
        <div class="box-body table-responsive" style="display: flex; justify-content: space-around; flex-wrap: wrap">
            <div class="col-md-7">
                <button type="button" class="btn btn-primary" style="font-weight: bold; font-size: 1.1rem; margin-left: 11px" data-toggle="modal" data-target="#exampleModal" id="btnNuevaFacturacion">
                    Nueva Factura
                </button>
            </div>
            <div class="col-md-4">
                <div class="box-body">
                    <div class="form-group">
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="container2 container modal-dialog" id="modalTamano" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 style="text-align: center; font-weight: bold" class="modal-title" id="exampleModalLabel">Ingrese Factura</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="modal-body1">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Seleccione Periodo</label>
                                                        <asp:DropDownList runat="server" ID="ddlPeriodo" CssClass="form-control js-example-templating" Width="100%" />
                                                        <label style="margin-top:8px;">Factura a nombre de:</label>
                                                        <asp:DropDownList runat="server" ID="ddlCliente" CssClass="form-control" onchange="mostrarRUC(this);">
                                                            <asp:ListItem Text="Seleccione" Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <br>
                                                        <div id="bloqRUC">
                                                            <label>Nro. RUC</label>
                                                            <asp:TextBox runat="server" ID="txtRUC" CssClass="form-control solo-numero"></asp:TextBox>
                                                        </div>
                                                        <br>
                                                        <div id="bloqPDF">
                                                            <label>Adjunte PDF</label>
                                                        <label style="width: auto;" class="file-upload btn btn-success">
                                                            Ingrese el archivo PDF ...
                                                                                    <asp:FileUpload ID="archivoPDF" runat="server" accept=".pdf" />
                                                        </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button id="btnCancelar" type="reset" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                        <button id="btnRegistrar" type="button" class="btn btn-success">Registrar</button>
                                        <button id="btnActualizar" type="button" class="btn btn-success">Actualizar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-top:30px;">
                <table id="tbl_facturacion" class="content-table table-bordered table-hover text-center">
                    <thead>
                        <tr>
                            <th style="text-align: center;">DNI</th>
                            <th style="text-align: center;">SOCIO</th>
                            <th style="text-align: center;">COMISIÓN</th>
                            <th style="text-align: center;">FECHA REGISTRO FACTURA</th>
                            <th style="text-align: center;">FACTURA DE 3RO.</th>
                            <th style="text-align: center;">NRO RUC</th>
                            <th style="text-align: center;">OBSERVACION</th>
                            <th style="text-align: center;">MONTO DEL CANJE</th>
                            <th style="text-align: center;">PERIODO DEL CANJE</th>
                            <th style="text-align: center;">SALDO DISPONIBLE</th>
                            <th style="text-align: center;">ESTADO CANJE</th>
                            <th style="text-align: center;">ESTADO SALDO DISPONIBLE</th>
                            <th style="text-align: center;">CORREGIR INFORMACIÓN</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="js/bootstrap4.min.js" type="text/javascript"></script>
    <script src="js/file-uploadv1.js" type="text/javascript"></script>
    <script src="js/proyecto2/jqueryDataTablesPremioSocios.js" type="text/javascript"></script>
    <script src="js/proyecto2/estiloTablasPremioSocios.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.8.7/dist/sweetalert2.all.min.js" type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js" type="text/javascript"></script>
    <script src="js/jquery.numeric.js" type="text/javascript"></script>
    <script src="js/GestionarFacturacionCliente.js?v3" type="text/javascript"></script>
    <script>
        $(".js-example-templating").select2({
        });
        function pageLoad() {
            $('.solo-numero').numeric();
        }
    </script>
</asp:Content>
