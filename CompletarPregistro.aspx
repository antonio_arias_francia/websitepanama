﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="CompletarPregistro.aspx.cs" Inherits="SantaNaturaNetworkV3.CompletarPregistro" ClientIDMode="Static" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/r-2.2.7/datatables.min.css" />
        <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel="stylesheet" type="text/css" />
    <style>
        .table tbody tr td:last-child {
            display: flex;
            justify-content: center;
        }

        .sorting_1 {
            background-color: inherit !important;
        }

        .table thead tr th {
            text-align: center;
        }

        .table tbody tr td {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container" style="margin-top: 100px; margin-bottom: 50px">
         <h1 style="text-align: center; margin-bottom: 50px">Completar Preregistro</h1>
        <div class="row">
            <div class="col-lg-12">
                <table id="table_id" class="table table-condensed display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>FECHA</th>
                            <th>DOCUMENTO</th>
                            <th>NOMBRES</th>
                            <th>APELLIDOS</th>
                            <th>UPLINE</th>
                            <th>PAQUETE</th>
                            <th>COMPRAR</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.23/r-2.2.7/datatables.min.js"></script>
    <script type="text/javascript" src="js/CompletarPreRegistro.js"></script>
    <script>
        $(document).ready(function () {
            
        });
    </script>
</asp:Content>
