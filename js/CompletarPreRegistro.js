﻿var tabla;

tabla = $('#table_id').dataTable({
    responsive: true
});

sendDataAjax();

function sendDataAjax() {
    $.ajax({
        type: "POST",
        url: "CompletarPregistro.aspx/ListarPreRegistro",
        data: {},
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            addRowDT(data.d);
        }
    });
}

function addRowDT(obj) {
    tabla.fnClearTable();
    for (var i = 0; i < obj.length; i++) {
        tabla.fnAddData([
            obj[i].FechaRegistro,
            obj[i].Documento,
            obj[i].Nombres,
            obj[i].Apellidos,
            obj[i].Nombres_Upline,
            obj[i].Paquete,
            '<button title="Comprar" type="button" class="btn btn-primary btn-comp"><i class="fas fa-store"></i></button>',
            obj[i].IdPaquete,
            obj[i].IdCliente
        ]);
    }
}

$(document).on('click', '.btn-comp', function (e) {
    e.preventDefault();
    var row2 = $(this).parent().parent()[0];
    var datax = tabla.fnGetData(row2);
    console.log(datax);
    id2 = datax[8];
    localStorage['STipoCompra'] = datax[7];
    GuardarDatosPreregis(id2);
});

function GuardarDatosPreregis(idcliente) {
    var obj = JSON.stringify({
        idCliente: idcliente
    });
    $.ajax({
        type: "POST",
        url: "CompletarPregistro.aspx/GuardarDatosPreRegis",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            location.href = "TiendaSN.aspx";
        }
    });
}